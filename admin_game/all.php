<?php
$myTitle = "All Players";
require '../includes/header.inc.php';

require '../classes/DB.class.php';
require '../classes/model/AdminAllPlayersM.class.php';
require '../classes/controller/AdminAllPlayersC.class.php';
//require '../classes/view/UserDataSelfV.class.php';

$regularUserObj1 = new AdminAllPlayersC();
$allPlayers = $regularUserObj1->GetAllPlayers();

foreach ($allPlayers as $p) { ?>

<div class="mb-5"></div>
<section class="alert alert-success container-fluid d-flex justify-content-center align-items-center text-center">
    <div class="col-4">
        <div class="fs-3 mb-2">New Player </div>
        <div><b>ID:</b> <?php echo $p['DB_personage_id']; ?> </div>
        <div><b>Nickname:</b> <?php echo $p['DB_personage_nickname']; ?> </div>
        <div><b>Type:</b> <?php echo $p['DB_personage_type']; ?> </div>
        <div><b>HP</b> <?php echo $p['DB_personage_hp']; ?> </div>
        <div><b>Power:</b> <?php echo $p['DB_personage_power']; ?> </div>
    </div>
</section>

<?php } ?>

<?php
require '../includes/footer.inc.php';
?>