<?php
$myTitle = "Admin Create Player";
require '../includes/header.inc.php';
?>




<section class="container-fluid d-flex justify-content-center align-items-center text-center">

    <div class="col-4">

        <div class="fs-3 mb-2">Admin can Create New Player</div>

        <form action="../includes/adm_create_new_player.inc.php" method="post">
            <div>
                <input name="gNewPlayerNickname" type="text" class="form-control" placeholder="Nickname">
            </div>

            <div>
                <input name="gNewPlayerType" type="text" class="form-control" placeholder="type">
            </div>

            
            <div>
                <input name="gNewPlayerHP" type="text" class="form-control" placeholder="HP">
            </div>

            <div class="mb-3">
                <input name="gNewPlayerPower" type="text" class="form-control" placeholder="Power">
            </div>

            <button type="submit" name="submitNewPlayer" class="btn btn-primary">Create New Player</button>
        </form>

</section>




<?php
require '../includes/footer.inc.php';
?>