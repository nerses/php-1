<?php

// C means Controller
class Add_C extends Add_M {
    private $p_user_id;
    private $p_couple_id;
    private $p_heroName;
    private $p_heroHP;
    private $p_heroPower;

    public function __construct($userid, $userPersonageName, $heroName, $heroHP, $heroPower)
    {
        $this->p_user_id = $userid;
        $this->p_couple_id = $userPersonageName;
        $this->p_heroName = $heroName;
        $this->p_heroHP = $heroHP;
        $this->p_heroPower = $heroPower;
        //exit($userPersonageName);
    }

    public function UserChoseHero()
    {
        $this->prpUserChoseHero($this->p_user_id, $this->p_couple_id, $this->p_heroName, $this->p_heroHP,$this->p_heroPower);
    }

}




?>