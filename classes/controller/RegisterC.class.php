<?php

// C means Controller
class RegisterC extends RegisterM {
    // theses are my RegisterController class's Properties
    // P means Property
    private $pseudo_P;
    private $email_P;
    private $password_P;

    // theses 3 Properties are equal to Data from User
    // Once I create new Object
    public function __construct($pseudoReg, $emailReg, $passwordReg)
    {
        $this->pseudo_P = $pseudoReg;
        $this->email_P = $emailReg;
        $this->password_P = $passwordReg;
    }


    public function CreateNewUserInDB()
    {
        $this->preparationToCreateNewUserInDB($this->pseudo_P, $this->email_P, $this->password_P);
    }



}


?>