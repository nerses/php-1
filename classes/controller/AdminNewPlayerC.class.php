<?php

// C means Controller
class AdminNewPlayerC extends AdminNewPlayerM {
    private $p_playerNickname;
    private $p_playerType;
    private $p_playerHP;
    private $p_playerPower;

    public function __construct($gNewPlayerNickname, $gNewPlayerType, $gNewPlayerHP, $gNewPlayerPower)
    {
        $this->p_playerNickname = $gNewPlayerNickname;
        $this->p_playerType = $gNewPlayerType;
        $this->p_playerHP = $gNewPlayerHP;
        $this->p_playerPower = $gNewPlayerPower;
    }

    public function AdminCreateNewPlayer()
    {
        $this->prpAdminCreateNewPlayer($this->p_playerNickname, $this->p_playerType, $this->p_playerHP, $this->p_playerPower);
    }

}




?>