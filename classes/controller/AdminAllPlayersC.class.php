<?php

class AdminAllPlayersC extends AdminAllPlayersM
{

    public function GetAllPlayers($php_personage_id)
    {
        $getAll = $this->pGetAllPlayers($php_personage_id);
        return $getAll;
    }



    public function GetNicknameSelect()
    {
        $getAllNicknameSelect = $this->pGetNicknameSelect();


        $s = "<select class='form-select' name='userPersonageName'>";
        $s .= "<option selected>Choose your Personage:</option>";
        for ($i = 0; $i < count($getAllNicknameSelect); $i++) {

            //echo '<pre>';
            //echo ($getAllNicknameSelect[$i]["DB_personage_nickname"];
            //echo '</pre>';

            $s .= '<option value="' . $getAllNicknameSelect[$i]["DB_personage_id"] . '">' . $getAllNicknameSelect[$i]["DB_personage_nickname"] . '</option>';
        }
        $s .= "</select>";
        

        // $s = "<select class='form-select' name='" . $selectName . "'" . $text . ">";
        // //  exit($sql);
        // if ($result->num_rows > 0) {
        //     while ($row = $result->fetch_assoc()) {
        //         $s .= '<option ' . ($defaultValue == $row["$fildId"] ? "selected" : "") . ' value="' . $row["$fildId"] . '">' . $row["$fildName"] . '</option>';
        //     }
        // }
        // $s .= "</select>";

        // return $s;

        // this returns select with all DB_personage_nickname
        return $s;
    }
}
