<?php

class AdminAllPlayersM extends DB
{

    // p means prepare to
    protected function pGetAllPlayers($personage_id = "")
    {
        $pdo = $this->connectDB();

        if ($personage_id == "") {
            $sql = "SELECT * FROM t_personage;";
        } else {
            $sql = "SELECT * FROM t_personage WHERE DB_personage_id = $personage_id";
        }

        $statement = $pdo->prepare($sql);
        $statement->execute();
        $allUser = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $allUser;
    }

    protected function pGetNicknameSelect()
    {
        $pdo = $this->connectDB();

        $sql = "SELECT DB_personage_id, DB_personage_nickname FROM t_personage ORDER BY DB_personage_nickname ASC";

        $statement = $pdo->prepare($sql);
        $statement->execute();
        $allNickname = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $allNickname;

        //$result = $conn->query($sql);

    }




    // protected function getSelect($conn, $tableName, $fildName, $fildId, $selectName, $defaultValue = 0, $text = "")
    // {
    //     $sql = "SELECT $fildId, $fildName FROM $tableName ORDER BY $fildName ASC";
    //     $result = $conn->query($sql);

    //     $s = "<select class='form-select' name='" . $selectName . "'" . $text . ">";
    //     //  exit($sql);
    //     if ($result->num_rows > 0) {
    //         while ($row = $result->fetch_assoc()) {
    //             $s .= '<option ' . ($defaultValue == $row["$fildId"] ? "selected" : "") . ' value="' . $row["$fildId"] . '">' . $row["$fildName"] . '</option>';
    //         }
    //     }
    //     $s .= "</select>";

    //     return $s;
    // }
    


}
