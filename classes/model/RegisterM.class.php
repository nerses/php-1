<?php
//Model

// Purpose add / create new users in my DataBase
// M means Model
class RegisterM extends DB
{

    // I have public __construct() in my RegisterController, so 
    // I can use RegisterController's 3 private Properties 
    protected function preparationToCreateNewUserInDB($pseudo_P, $email_P, $password_P)
    {
        // my Register class extends from DB class
        // connectDB() Method is protected, so I use here
        
        // first I connect to my Database
        $pdo = $this->connectDB();

        // prepare() uses prepared statements a made the query, prepare is better vs sql injection
        // query() performs a simple query, with each call a query is sent to the database
        
        $sql = "INSERT INTO t_user_reg (DB_reg_pseudo, DB_reg_email, DB_reg_password) VALUES (?,?,?)";
        // I prepare my query with prepare() (query is запрос) 
        $statement = $pdo->prepare($sql);
        // I hash password before I will add
        $hashedPwd = password_hash($password_P, PASSWORD_DEFAULT);
        
        
        //I have more than one piece of data pseudo, email, password so I will need to insert this as an array()
        //and I want to check if this Fails so I add this symbol "!" to my statement like !$statement
        if (!$statement->execute(array($pseudo_P, $email_P, $hashedPwd))) {
            //when this will fails, I want to not Run the Rest of my code
            //so I want to close my Statement 

            //statement equals to null, this is how I can delete the statement entirely
            $statement = null;
            
            //then I want to create a header function which will send the user back to the Front page with error message
            header("location: ../index.php?Error=RegStmt1Failed");
            //after I want to exit this entire script
            exit();
        }

    }




}







        // $statement = $this->connectDB()->prepare('INSERT INTO t_user_reg (DB_reg_pseudo, DB_reg_email, DB_reg_password) VALUES (?, ?, ?);');
        // $statement->execute(array($pseudoProperty, $emailProperty, $passwordProperty));

