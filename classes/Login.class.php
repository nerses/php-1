<?php
//Model

// Purpose add / create new users in my DataBase
class Login extends DB
{

    protected function preparationToLoginUser($pseudoLogin, $passwordLogin)
    {
        // first I connect to my Database
        $pdo = $this->connectDB();
        // I select All like than I can turn them session Variables and use everywhere
        $sql = 'SELECT * FROM t_user_reg WHERE DB_reg_pseudo = :usr;';
        // I prepare my query
        $statement = $pdo->prepare($sql);
        $statement->bindParam(":usr", $pseudoLogin, PDO::PARAM_STR);
        $statement->execute();

        // results returns what ever I will give in my SELECT
        $results = $statement->fetch(PDO::FETCH_ASSOC);


        // this is hashed password from my DB
        $hashedPwd = $results["DB_reg_password"];
        // check if users's password Match
        $chechPwd = password_verify($passwordLogin, $hashedPwd);

        if (empty($pseudoLogin)) {
            $statement = null;
            header("Location: ../files/login.php?error=EmptyPseudo");
            exit();
        }
        if (empty($passwordLogin)) {
            $statement = null;
            header("Location: ../files/login.php?error=EmptyPwd");
            exit();
        }




        if ($chechPwd == true) {
            // login user
            session_start();
            // session Variables
            $_SESSION["id_session"] = $results["DB_reg_id"];
            $_SESSION["pseudo_session"] = $results["DB_reg_pseudo"];
            $_SESSION["email_session"] = $results["DB_reg_email"];
            $_SESSION["p_session"] = $results["DB_reg_password"];

        } else {
            $statement = null;
            header("Location: ../files/login.php?error=WrongPassword");
            exit();
        }
    }
}





        // echo '<pre>';
        // var_dump($results);
        // echo '</pre>';
        // exit();