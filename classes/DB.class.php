<?php

// Purpose connect to my DataBase with PDO
class DB
{
    protected function connectDB()
    {
        try {
            $pdo = new PDO('mysql:dbname=db_mybook;host=127.0.0.1', 'root', '');
            return $pdo;
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}
