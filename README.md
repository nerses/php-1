## Disclaimer
- I'm a junior developer, so don't expect from me to make Second FaceBook or Second Google Search Engine in 24 houres.
- Do I have permission to "not knowing every programming languages of this world" ?

## Learning and Practicing
- I have many small projects done but I have never made a project with all the features combined.
- I will start with simple way than I will improve my code.
- I would like to understand better my code, so No Frameworks ! 
- However I am here to improve my backend, so I will save some time on design / style, so YES for Bootstrap and similar Frameworks.

## Here is my goal
- Register / Login / Blog / Admin / E-commerce
- MVC / PHP OOP / PDO / Ajax / JS / HTML / CSS



## Updates
- 01/05/2022
- Add lots of stuff
- Add Edit option, but need be fixed
- Add Hero table and Now User can choose a Hero
- Some Style Bootstrap
- Some Ajax Use
- If there is no updates that doesn't means that I forget this project

