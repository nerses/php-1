<?php
$myTitle = "Registration Page";
require '../includes/header.inc.php';
?>




<section class="container-fluid d-flex justify-content-center align-items-center text-center">

    <div class="col-4">

        <div class="fs-3 mb-2">Create New Account</div>

        <form action="../includes/register.inc.php" method="post">
            <div>
                <input name="pseudoReg" type="text" class="form-control" placeholder="your pseudo">
            </div>

            <div>
                <input name="emailReg" type="text" class="form-control" placeholder="your e-mail">
            </div>

            <div class="mb-3">
                <input name="passwordReg" type="text" class="form-control" placeholder="your password">
            </div>

            <button type="submit" name="submitReg" class="btn btn-primary">Registration</button>
        </form>

</section>




<?php
require '../includes/footer.inc.php';
?>