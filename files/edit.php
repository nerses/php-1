<?php
$myTitle = "Modify My Profile";
require '../includes/header.inc.php';

require '../classes/DB.class.php';
require '../classes/model/UserDataSelfM.class.php';
require '../classes/controller/UserDataSelfC.class.php';
//require '../classes/view/UserDataSelfV.class.php';

$regularUserObj2 = new UserDataSelfC();
$myData2 = $regularUserObj2->GetSelfData();

foreach ($myData2 as $b) { ?>

<div class="mb-5"></div>
<section class="container-fluid d-flex justify-content-center align-items-center text-center">

    <div class="col-4">
    <!-- DB_reg_id -->
        <div class="fs-3 mb-2">Modify My Profile</div>

        <form action="../includes/edit.inc.php" method="post">

            <div>
                <input name="pseudoEdit" type="text" class="form-control" value="<?php echo $b['DB_reg_pseudo']; ?>">
            </div>

            <div class="mb-3">
                <input name="emailEdit" type="text" class="form-control" value="<?php echo $b['DB_reg_email']; ?>">
            </div>

            <button type="submit" name="submitEdit" class="btn btn-warning">Change</button>
        </form>

</section>


<?php
}

?>

<?php
require '../includes/footer.inc.php';
?>