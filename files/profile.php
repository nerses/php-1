<?php
$myTitle = "My Profile";
require '../includes/header.inc.php';

require '../classes/DB.class.php';
require '../classes/model/UserDataSelfM.class.php';
require '../classes/controller/UserDataSelfC.class.php';
//require '../classes/view/UserDataSelfV.class.php';

$regularUserObj1 = new UserDataSelfC();
$myData = $regularUserObj1->GetSelfData();

foreach ($myData as $a) { ?>

<div class="mb-5"></div>
<section class="alert alert-success container-fluid d-flex justify-content-center align-items-center text-center">
    <div class="col-4">
        <div class="fs-3 mb-2">Information about my account </div>
        <div><b>My ID:</b> <?php echo $a['DB_reg_id']; ?> </div>
        <div><b>My Username:</b> <?php echo $a['DB_reg_pseudo']; ?> </div>
        <div><b>My Email:</b> <?php echo $a['DB_reg_email']; ?> </div>
    </div>
</section>

<?php } ?>

<?php
require '../includes/footer.inc.php';
?>