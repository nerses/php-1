<?php
if (isset($_POST['submitEdit'])) {

    session_start();
    // This is Data submited from user from my FORM
    $pseudoEdit = $_POST['pseudoEdit'];
    $emailEdit = $_POST['emailEdit'];
  
    //connect to my DataBase
    require '../classes/DB.class.php';
    require '../classes/model/UserEditM.class.php';
    require '../classes/controller/UserEditC.class.php';

    $regularUserEdit = new UserEditC($pseudoEdit, $emailEdit);
    $regularUserEdit->EditUserData();

    //var_dump($r);
    header('Location: ../files/profile.php');
}
