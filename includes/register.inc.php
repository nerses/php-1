<?php

if (isset($_POST['submitReg'])) {

    // This is Data submited from user from my FORM
    $pseudoReg = $_POST['pseudoReg'];
    $emailReg = $_POST['emailReg'];
    $passwordReg = $_POST['passwordReg'];

    //connect to my DataBase
    include '../classes/DB.class.php';
    include '../classes/model/RegisterM.class.php';
    include '../classes/controller/RegisterC.class.php';
    
    $regularUser = new RegisterC($pseudoReg, $emailReg, $passwordReg);
    $regularUser->CreateNewUserInDB();

    header('Location: ../index.php?error=None');
}
