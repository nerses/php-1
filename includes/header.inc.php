<?php
session_start();
?>
<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- ajax jquery -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <!-- Bootstrap Icons -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">

    <!-- Fontawesome CSS -->
    <script src="https://kit.fontawesome.com/3ac3846f52.js" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="../style/style.css">
    <title>
        <?php echo $myTitle; ?>
    </title>
</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">

                    <li class="nav-item">
                        <a class="nav-link" href="../index.php">Home</a>
                    </li>

                    <?php
                    if (isset($_SESSION['id_session'])) {
                    ?>

                        <li class="nav-item">
                            <a class="nav-link" href="/files/profile.php">My Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/includes/logout.inc.php">Logout</a>
                        </li>


                    <?php
                    } else {
                    ?>

                        <li class="nav-item">
                            <a class="nav-link" href="/files/register.php">Registration</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/files/login.php">Login</a>
                        </li>

                    <?php
                    }
                    ?>






                    <li class="nav-item">
                        <a class="nav-link" href="#">Link</a>
                    </li>

                </ul>

            </div>
        </div>
    </nav>




    <?php
    if (isset($_SESSION['id_session'])) {
    ?>


        <ul class="nav justify-content-center">

            <li class="nav-item">
                <a class="nav-link" href="../files/profile.php">See My Account</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../files/edit.php">Modify My Profile</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="../game/first.php">Game</a>
            </li>

        </ul>


        
        <ul class="nav justify-content-center alert alert-primary">

            <li class="nav-item">
                <a class="nav-link" href="../admin_game/first.php">Game Admin</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../admin_game/add.php">Add</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="../admin_game/all.php">See All</a>
            </li>

        </ul>


    <?php
    } else {
    ?>


    <?php
    }
    ?>