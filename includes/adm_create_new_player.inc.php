<?php

if (isset($_POST['submitNewPlayer'])) {

    // This is Data submited from user from my FORM
    $gNewPlayerNickname = $_POST['gNewPlayerNickname'];
    $gNewPlayerType = $_POST['gNewPlayerType'];
    $gNewPlayerHP = $_POST['gNewPlayerHP'];
    $gNewPlayerPower = $_POST['gNewPlayerPower'];

    //connect to my DataBase
    include '../classes/DB.class.php';
    include '../classes/model/AdminNewPlayerM.class.php';
    include '../classes/controller/AdminNewPlayerC.class.php';
    
    $adminCreateNewPlayer = new AdminNewPlayerC($gNewPlayerNickname, $gNewPlayerType, $gNewPlayerHP, $gNewPlayerPower);
    $adminCreateNewPlayer->AdminCreateNewPlayer();

    header('Location: ../index.php?error=None');
}
