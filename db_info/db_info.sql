
CREATE DATABASE db_mybook;

USE db_mybook;

-- registeated user
CREATE TABLE t_user_reg (
    DB_reg_id int(11) PRIMARY KEY AUTO_INCREMENT NOT NULL,
    DB_reg_pseudo VARCHAR(20) NOT NULL,
    DB_reg_email VARCHAR(30) NOT NULL,
    DB_reg_password LONGTEXT NOT NULL

);


-- USE db_mybook;
-- t_game_admin_players
-- here Admin add / create New Charachters / players
-- description / age / firstname / lastname / type: Knight, Hunter, Hero, Monster
CREATE TABLE t_personage (
    DB_personage_id int(11) PRIMARY KEY AUTO_INCREMENT NOT NULL,

    DB_personage_nickname VARCHAR(20) NOT NULL,
    DB_personage_type VARCHAR(20) NOT NULL,
    DB_personage_hp VARCHAR(20) NOT NULL,
    DB_personage_power VARCHAR(20) NOT NULL

);

-- USE db_mybook;
-- here Users choose their personage 
CREATE TABLE t_user_personage (
    DB_user_personage_id int(11) PRIMARY KEY AUTO_INCREMENT NOT NULL,
    DBreg_couple_id int(11) NOT NULL, -- this 2 couple will be Unique         -- 4 Tom from register table
    DBpersonage_couple_id int(11) NOT NULL, -- this 2 couple will be Unique   -- 7 Knight
    DB_user_personage_name VARCHAR(20) NOT NULL, -- this will be Unique       -- MySuper Unique Name
    DB_user_personage_hp VARCHAR(20) NOT NULL,
    DB_user_personage_power VARCHAR(20) NOT NULL

);